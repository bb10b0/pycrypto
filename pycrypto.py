import os
import uuid
import secrets

from Crypto.PublicKey import RSA as rsa
from Crypto.Random import get_random_bytes
from Crypto.Cipher import AES, PKCS1_OAEP


class PyCrypto():
    def __init__(self, crypto_id=None, pri_key=None, pub_key=None):
        # 64kb
        self.buffer_size = 65536
        self.crypto_id = crypto_id or str(uuid.uuid4())
        self.private_key_file = pri_key or self.crypto_id + '/private.pem'
        self.public_key_file = pub_key or self.crypto_id + '/public.pem'
        if not pri_key and not pub_key:
            self.generate_keypair()

    def generate_keypair(self):
        """Generate key pair."""
        # create dir
        os.mkdir(self.crypto_id)
        # private key gen
        key = rsa.generate(2048)
        private_key = key.export_key()
        file_out = open(self.private_key_file, "wb")
        file_out.write(private_key)

        # pulic key gen
        public_key = key.publickey().export_key()
        file_out = open(self.public_key_file, "wb")
        file_out.write(public_key)

    def encrypt(self, file_path):
        """Encrypt file on given path."""
        with open(file_path, 'rb') as file_in:
            with open(self.crypto_id + '/encrypted_data.bin', "wb") as file_out:
                recipient_key = rsa.import_key(
                    open(self.public_key_file).read()
                )
                session_key = get_random_bytes(16)

                # Encrypt the session key with the public RSA key
                cipher_rsa = PKCS1_OAEP.new(recipient_key)
                enc_session_key = cipher_rsa.encrypt(session_key)

                # Encrypt the data with the cipher.AES session key
                cipher_aes = AES.new(session_key, AES.MODE_EAX)

                buffer = file_in.read(self.buffer_size)
                while len(buffer) > 0:
                    ciphered_bytes = cipher_aes.encrypt(buffer)
                    tag = cipher_aes.digest()
                    [file_out.write(x) for x in
                        (enc_session_key, cipher_aes.nonce, tag,
                         ciphered_bytes)]
                    buffer = file_in.read(self.buffer_size)

    def decrypt(self, file_path, private_key_path):
        """
        decrypt file on given file_path with private_key_path. Does not assume
        that private key was provided on instantiation.
        """
        with open(self.crypto_id + '/encrypted_data.bin', 'rb') as file_in:
            with open(self.crypto_id + '/decrypted', 'wb') as file_out:
                private_key = rsa.import_key(open(private_key_path).read())

                enc_session_key, nonce, tag, ciphertext = \
                    [file_in.read(x)
                     for x in (private_key.size_in_bytes(), 16, 16, -1)]

                # Decrypt the session key with the private RSA key
                cipher_rsa = PKCS1_OAEP.new(private_key)
                session_key = cipher_rsa.decrypt(enc_session_key)

                # Decrypt the data with the AES session key
                cipher_aes = AES.new(session_key, AES.MODE_EAX, nonce)
                data = cipher_aes.decrypt_and_verify(ciphertext, tag)
                file_out.write(data)


if __name__ == "__main__":
    # create instance and encrypt
    print('[+] pc created!')
    pc = PyCrypto()
    print(pc.crypto_id)
    print(pc.private_key_file)
    print(pc.public_key_file)
    print('[+] Begin encrypting!')
    pc.encrypt('test.txt')
    print('[+] File encrypted!')

    # create instance w/ same id
    pc2 = PyCrypto(
        crypto_id=pc.crypto_id,
        pri_key=pc.private_key_file,
        pub_key=pc.public_key_file
    )
    print('[+] pc2 created!')
    print(pc2.crypto_id)
    print(pc2.private_key_file)
    print(pc2.public_key_file)
    pc2.decrypt(
        file_path=pc2.crypto_id + '/encrypted_data.bin',
        private_key_path=pc2.private_key_file
    )
    print('[+] pc2 decrypted!')
